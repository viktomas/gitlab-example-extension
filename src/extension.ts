// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { GitService } from './gitService';
import { GitLabUrlService } from './gitlabUrlService';

const openUrl = (url: string) => {
  vscode.commands.executeCommand('vscode.open', vscode.Uri.parse(url));
};

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
  // Use the console to output diagnostic information (console.log) and errors (console.error)
  // This line of code will only be executed once when your extension is activated
  console.log('Congratulations, your extension "gitlab-example-extension" is now active!');

  // The command has been defined in the package.json file
  // Now provide the implementation of the command with registerCommand
  // The commandId parameter must match the command field in package.json
  let disposable = vscode.commands.registerCommand(
    'gitlab-example-extension.permalink',
    async () => {
      if (!vscode.workspace.rootPath) {
        console.error('There is no open workspace');
        return;
      }
      const gitService = new GitService(vscode.workspace.rootPath);
      const repoUrl = await gitService.repositoryUrl();
      if (!repoUrl) {
        console.error('no remote ref');
        return;
      }
      const editor = vscode.window.activeTextEditor;
      if (!editor) {
        console.error('no open editor');
        return;
      }
      const fullFilePath = editor?.document.uri.path || '/';
      const workspacePath = vscode.workspace.rootPath;
      const filePath = fullFilePath.replace(workspacePath ?? '', '');
      const commit = await gitService.latestCommitForFile(fullFilePath);
      if (!commit) {
        console.error('no commit');
        return;
      }
      const start = editor.selection.start.line;
      const end = editor.selection.end.line;
      const instanceUrl = vscode.workspace.getConfiguration('gitlab').instanceUrl;
      const urlService = new GitLabUrlService(instanceUrl);
      const url = urlService.permalink(repoUrl, filePath, commit, start, end);
      openUrl(url);
    },
  );

  context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {}
