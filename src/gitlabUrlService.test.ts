import { RepositoryUrl } from './gitService';
import { GitLabUrlService } from './gitlabUrlService';
import * as assert from 'assert';

describe('GitLabUrlService', () => {
  it('generates permalink url', () => {
    const urlService = new GitLabUrlService('https://gitlab.com');
    const result = urlService.permalink(
      new RepositoryUrl('git@gitlab.com:gitlab-org/gitlab.git'),
      '/README.md',
      { sha: 'abcdef1234567890' },
      5,
      20,
    );
    assert.strictEqual(
      result,
      'https://gitlab.com/gitlab-org/gitlab/-/blob/abcdef1234567890/README.md#L5-20',
    );
  });
});
