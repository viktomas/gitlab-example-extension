import * as createSimpleGit from 'simple-git/promise';
import { SimpleGit } from 'simple-git/promise';

export interface Commit {
  readonly sha: string;
}

export class RepositoryUrl {
  private static readonly REGEXP = /^git@gitlab.com:(.*)\.git$/;
  private rawUrl: string;
  constructor(rawUrl: string) {
    const matchResult = rawUrl.match(RepositoryUrl.REGEXP);
    if (!matchResult) {
      throw new Error('Non-GitLab remote');
    }
    this.rawUrl = rawUrl;
  }
  toString(): string {
    return this.rawUrl;
  }
  get path(): string {
    const matchResult = this.rawUrl.match(/^git@gitlab.com:(.*)\.git$/) as RegExpMatchArray;
    const [, path] = matchResult;
    return path;
  }
}

export class GitService {
  private simpleGit: SimpleGit | undefined;
  constructor(workspacePath: string) {
    this.simpleGit = createSimpleGit(workspacePath);
  }

  async repositoryUrl(): Promise<RepositoryUrl | undefined> {
    const remote = (await this.simpleGit?.getRemotes(true))?.pop();
    return remote && new RepositoryUrl(remote.refs.fetch);
  }

  async latestCommitForFile(filePath: string): Promise<Commit | undefined> {
    const log = await this.simpleGit?.log({ file: filePath });
    const sha = log?.latest.hash;
    return sha ? { sha } : undefined;
  }
}
