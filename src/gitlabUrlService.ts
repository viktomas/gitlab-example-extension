import { Commit, RepositoryUrl } from './gitService';

export class GitLabUrlService {
  private baseUrl: string;

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl;
  }
  permalink(
    repoUrl: RepositoryUrl,
    filePath: string,
    commit: Commit,
    start: number,
    end: number | undefined,
  ): string {
    return `${this.baseUrl}/${repoUrl.path}/-/blob/${commit.sha}${filePath}#L${start}${
      end ? `-${end}` : ''
    }`;
  }
}
