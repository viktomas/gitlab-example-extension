# gitlab-example-extension

This project offers a single command: "Open GitLab Permalink". The main goal of this project is to show how to host a VS Code extension on GitLab.

## Features

- **Open GitLab Permalink** - this command will open GitLab repository website and highlights the current selection.

## Repository

The purpose of this project is to build on top of the [VS Code generated extension](https://code.visualstudio.com/api/get-started/your-first-extension) and provide automation for linting, testing and publishing the extension.

## Automation

### Linting

Task: `npm run lint`

Linting is done using `eslint` and `prettier`. Code can be formatted/fixed automatically by running `npm run lint-fix`

### Unit Testing

Task: `npm run test-unit`

Unit testing is done by `mocha` and `assert`. **Only files that are not importing `vscode` can be unit tested**.

The `*.text.ts` name format for both integration tests and unit tests is a bit confusing and potentially subject to change.

### Integration testing

Task: `npm run test-integration`

These tests need an X server and vscode binary to run. More information is in the [official documentation on testing](https://code.visualstudio.com/api/working-with-extensions/testing-extension)

### Publishing

1. Get a personal access token
   1. [For Visual Studio Marketplace](https://code.visualstudio.com/api/working-with-extensions/publishing-extension#get-a-personal-access-token)
   1. [For open-vsx](https://github.com/eclipse/openvsx/wiki/Publishing-Extensions#1-create-an-access-token)
1. Add the tokens as [environment variable to your project CI](https://docs.gitlab.com/ee/ci/variables/#custom-environment-variables-of-type-variable)
   1. `AZURE_ACCESS_TOKEN` - Visual Studio Marketplace token
   1. `OPENVSX_ACCESS_TOKEN` - open-vsx token
1. Change extension version, name, and publisher in `package.json` commit, push and tag the commit.
1. On tag pipelines, you can now manually trigger the publish step.

## Extension Settings

- `gitlab.instanceUrl`: the base URL for your GitLab instance, defaults to `https://gitlab.com`
